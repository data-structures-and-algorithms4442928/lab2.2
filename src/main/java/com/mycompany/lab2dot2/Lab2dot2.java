package com.mycompany.lab2dot2;

import java.util.Arrays;

public class Lab2dot2 {
    public int removeElement(int[] nums, int val) {
        int k = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[k] = nums[i];
                k++;
            }
        }
        return k;
    }

    public static void main(String[] args) {
        int[] nums = {3,2,2,3};
        int val = 3;
        Lab2dot2 removeElementObj = new Lab2dot2();
        int k = removeElementObj.removeElement(nums, val);
        System.out.println("Output: " + k + ", nums = " + Arrays.toString(nums));

        int underscore = 99;
        for (int i = k; i < nums.length; i++) {
            nums[i] = underscore;
        }

        System.out.println("After filling with underscores: " + Arrays.toString(nums));
    }
}
